<?php
    /**
     * Main PHP logic file for handling manipulation of the todo list
     *
     * This file holds all of the functions necessary to manipulate
     * the todo list. If anything manipulates the todo list, it belongs
     * in this file.
     *
     * @author Nathan Heffley
     */

    /**
     * Holds all of the todo items
     */
    $todoList = array();

    /**
     * Add item to the $todoList array
     */
    function addTodoItem($todoItem)
    {
        // TODO: addTodoList() logic
    }

    /**
     * Remove item from the $todoList array
     */
    function removeTodoItem($todoItem)
    {
        // TODO: removeTodoList() logic
    }

    /**
     * Display the $todoList array
     */
    function displayTodoList()
    {
        // TODO: displayTodoList() logic
    }
?>
