<?php
    require_once 'applications/todo/todo.php';

    if (isset($_POST["todoItem"])) {
        $todoItem = htmlspecialchars($_POST["todoItem"]);
        addTodoItem($todoItem);
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>PHPStuff</title>
    </head>
    <body>
        <h1>To-Do List</h1>

        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
            <input type="text" name="todoItem" />
            <input type="submit" name="submit" value="Add to To-Do List" />
        </form>

        <h3>Things to do:</h3>
    </body>
</html>
